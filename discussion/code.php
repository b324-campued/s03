<?php 
// The Object-Oriented Approach

// Procedural Programming
// Before OOP become popular there was an approach called procedural programming (normal programming we do)
// In this approach programs are written as series step by step instructions (TOP to BOTTOM)
// This approach emphasized more on the programs logic than the structure of the data handled.

// OOP Approach
// This approach focus more on how data is structured within the program and how is it going to be used.
// Function inside the object is called method
// Using this approach allow the developers to model real-world object, either physical or imaginary, such as employee details, building datam and bank accounts.
// Programming languages like c# and Java are some languages that heavily implements this approach

// Classes and Objects
// Classes - blueprints that define values and behaviors/methods (Constructor)
	// Create objects with same characteristics
// Objects - implementation of these classes
// Objects created from a class by creating instance


// Four Pillars of OOP
// 1. Inheritance - Derived classes are allowed to inherit variables/properties and methods from specific base class.
	// Parent class to child class
// 2. Abstraction - Only the objects features are visibe but the actual implementation are hidden
// 3. Encapsulation - Also known as data binding, it dictates that data must not be directly accessible to users but through a public function call property.
	// you need to bind data being access to variable then use it as you cant directly access it.
// 4. Polymorphism - Method inherited by a derived class cam be in same name but different numbers of parameters. The correct method to be called is determined at compile time based on the number and types of arguments passed. BUT in PHP does not differentiate methods based on the number or types of parameters during compilation. Instead, the last defined method with the same name will be used.



$hello = 'Hello World!';


$buildingObj = (object)[
	'name' => 'Caswynn Bldg',
	'floors' => 8,
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];
// Create an object using Class
// CapitalNaming for Class
class Building {
	// PHP does not enforce strict visibility for properties by default, which means you can access and modify both public and non-public (protected and private) properties from outside the class.  ( "loose typing" or "weak typing.)
	public $name;
    public $floors;
    public $address;
    public $zipCode;

	// A constructor is used during the creation of an object
	// Contruct function that will dictate values of properties 
	// space then double underscore
	// 
	public function __construct($name, $floors, $address, $zipCode){
		$this->name = $name;
		$this->floors = $floors;
		$this ->address = $address;
		$this ->zipCode = $zipCode;

	}

	public function printName() {
		return "The name of the building is $this->name.";
	}

	public function checkFloors() {
		return "$this->floors";
	}
}

$building = new Building('Caswynn Bld', 8, 'Timog Avenue, Quezon City', '2433');
$secondBuilding = new Building('Mark Bld', 8, 'Timog Avenue, Quezon City', '2433');


// Inheritance
// Inherit Building Class Object to Condominium
// Additions to child does not affect parent class object
class Condominium extends Building{
	public $rooms;

	public function __construct($name, $floors, $address, $zipCode, $rooms){
		$this->name = $name;
		$this->floors = $floors;
		$this ->address = $address;
		$this ->zipCode = $zipCode;
		$this ->rooms = $rooms;

	}

	// Polymorphism (Behavior of our object changes)
	// Change building string into condomominium
	public function printName() {
		return "The name of the condominium is $this->name.";
	}

	public function checkFloors() {
		return "$this->floors with $this->rooms";
	}

};

$condominium = new Condominium('Baguio Condominium', 50, 'Bakakeng, Baguio City', '2433', 500);



// Abstraction
abstract class Drink{
	public $name;

	public function __construct($name){
		$this->name = $name;
	}

	// We just put function here as parent but we don't know what it do for now until we create the inheritance
	public abstract function getDrinkName();
};


class Coffee extends Drink{
	public function getDrinkName(){
		return "The name of the coffee is $this->name";
	}
};


$kopiko = new Coffee('Kopiko');


?>